// PostP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
//#include "cv.h"
//#include "highgui.h"
//using namespace cv;
#include <iostream>
#include <fstream>
using namespace std;
#include "MeanShift2.hpp"//generating meanshift segmentation (https://github.com/qiankanglai/opencv.meanshift)
#include "GuidedFilter2.hpp"

#define leftPath						"imL.png"
#define rightPath						"imR.png"
#define superpixelPath					"imL.dat"//generating superpixel maps (https://ivrl.epfl.ch/research/superpixels)
#define dispPath						"input_disp.png"
#define dRange							15
#define SCALE							16

int AdaptiveSuperpixelSegmentation(int **comp)
{
	int label_num = 186;
	double threshold = 0.6;
	Mat Image = imread(leftPath);
	Mat Image_ms = Image.clone();
	int width = Image.cols, height = Image.rows;
	int sz = width * height;
	cvtColor(Image, Image, COLOR_BGR2GRAY);
	const int channels[1] = { 0 };

	const int histSize2[1] = { 5 };
	float hranges2[6] = { 0, 50, 80, 150, 230, 255 };
	const float* ranges2[1] = { hranges2 };

	//MeanShift
	cout << "start MeanShift.." << endl;
	double t_start_ms = (double)getTickCount();
	Mat dst_ms;
	cvtColor(Image_ms, dst_ms, COLOR_BGR2YCrCb);
	vector<Mat> YCbCr(3);
	split(dst_ms, YCbCr);
	equalizeHist(YCbCr[0], YCbCr[0]);
	Mat img_ms;
	merge(YCbCr, img_ms);
	cvtColor(img_ms, img_ms, COLOR_YCrCb2BGR);
	IplImage img_ms_Ipl = IplImage(img_ms);
	int regionCount = MeanShift(&img_ms_Ipl, comp);
	t_start_ms = ((double)getTickCount() - t_start_ms) / getTickFrequency();

	//SuperPixel + Mask_MeanShift
	cout << "+SuperPixels.." << endl;
	vector<int> thres;
	Mat mask(Image.rows, Image.cols, CV_8U, Scalar::all(0));
	int *labels = new int[sz];
	ifstream in;
	in.open(superpixelPath, ios::binary);
	if (!in.is_open())
	{
		cout << "error opening file" << endl;
		return -1;
	}
	for (int i = 0; i < sz; i++)
		in.read((char*)&labels[i], sizeof(int));
	in.close();
#pragma omp for
	for (int la = 0; la < label_num; la++)
	{
		mask = Scalar::all(0);
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
			{
				if (labels[i*width + j] == la)
					mask.at<uchar>(i, j) = 1;
			}
		MatND hist;
		calcHist(&Image, 1, channels, mask, hist, 1, histSize2, ranges2, false);
		double num_sum = sum(hist)[0];
		double num_min, num_max; minMaxIdx(hist, &num_min, &num_max);
		if (num_max / num_sum < threshold)
			thres.push_back(la);
	}
#pragma omp for
	for (int t = 0; t < thres.size(); t++)
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
			{
				if (labels[i*width + j] == thres[t])
					labels[i*width + j] = label_num + comp[i][j];
			}
#pragma omp for
	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++)
			comp[i][j] = labels[i*width + j];

	return 0;
}
Mat Findoutlier(Mat image2)
{
	int serc_rang = 7;
	int edgeThresh = 19;
	int wid = image2.cols;
	int hei = image2.rows;
	Mat occp = Mat::zeros(hei, wid, CV_8U);

	int **comp = new int *[hei];
	for (int i = 0; i<hei; i++)
		comp[i] = new int[wid];
	AdaptiveSuperpixelSegmentation(comp);

	//edge
	Mat edges;
	Mat image2_tmp = image2.clone(); image2_tmp *= SCALE;
	blur(image2_tmp, image2_tmp, Size(3, 3));
	Canny(image2_tmp, edges, edgeThresh, edgeThresh * 3, 3); //imshow("edge", edges);
#pragma omp for
	for (int i = 0; i<hei; i++)
		for (int j = 0; j<wid; j++)
		{
			if (edges.at<uchar>(i, j) == 255)
			{
				int fl = comp[i][j];
				bool check1 = comp[MAX(i - 1, 0)][j] == fl && comp[MIN(i + 1, hei - 1)][j] == fl && comp[i][MAX(j - 1, 0)] == fl && comp[i][MIN(j + 1, wid - 1)] == fl;
				bool check2 = comp[MAX(i - 2, 0)][j] == fl && comp[MIN(i + 2, hei - 1)][j] == fl && comp[i][MAX(j - 2, 0)] == fl && comp[i][MIN(j + 2, wid - 1)] == fl;
				bool check3 = comp[MAX(i - 3, 0)][j] == fl && comp[MIN(i + 3, hei - 1)][j] == fl && comp[i][MAX(j - 3, 0)] == fl && comp[i][MIN(j + 3, wid - 1)] == fl;
				if (check1 && check2 && check3)
					edges.at<uchar>(i, j) = 255;
				else
					edges.at<uchar>(i, j) = 0;
			}
		}
	//imshow("wred", edges);

	//search
	Mat edges_tmp = edges.clone();
#pragma omp for
	for (int i = 0; i<hei; i++)
		for (int j = 0; j<wid; j++)
		{
			if (edges.at<uchar>(i, j) == 255)
			{
				int sum_up = 0, sum_dn = 0;
				while ((i - sum_up >= 0) && (comp[i - sum_up][j] == comp[i][j]))
					sum_up++;
				while ((i + sum_dn<hei) && (comp[i + sum_dn][j] == comp[i][j]))
					sum_dn++;
				int sum_lt = 0, sum_rt = 0;
				while ((j - sum_lt >= 0) && (comp[i][j - sum_lt] == comp[i][j]))
					sum_lt++;
				while ((j + sum_rt<wid) && (comp[i][j + sum_rt] == comp[i][j]))
					sum_rt++;
				int min_wd = INT_MAX;
				if ((sum_up<min_wd) && (sum_up != 0))
					min_wd = sum_up;
				if ((sum_dn<min_wd) && (sum_dn != 0))
					min_wd = sum_dn;
				if ((sum_lt<min_wd) && (sum_lt != 0))
					min_wd = sum_lt;
				if ((sum_rt<min_wd) && (sum_rt != 0))
					min_wd = sum_rt;

				int wd = min_wd;
				int ind_direct = 0;
				while (1)
				{
					int countp[dRange] = { 0 };
					int ys = MAX(0, i - wd), ye = MIN(hei - 1, i + wd), xs = MAX(0, j - wd), xe = MIN(wid - 1, j + wd);
					for (int y = ys; y <= ye; y++)
					{
						for (int x = xs; x <= xe; x++)
						{
							int ind_tmp = image2.at<uchar>(y, x);
							if (ind_tmp>=dRange)
								occp.at<uchar>(i, j) = 1;
							else
								countp[ind_tmp - 1]++;
						}
					}
					int countmax = INT_MIN;
					for (int ck = 0; ck<dRange; ck++)
						if (countp[ck] != 0)
						{
							if (countp[ck]>countmax)
							{
								countmax = countp[ck];
								ind_direct = ck;
							}
						}
					if (countmax == (ye - ys + 1)*(xe - xs + 1) / 2)
						wd++;
					else
						break;
				}

				if (image2.at<uchar>(MAX(i - 1, 0), j) != image2.at<uchar>(MIN(i + 1, hei - 1), j))//vertical
				{
					if (image2.at<uchar>(MAX(i - 1, 0), j) == ind_direct + 1)//down
					{
						for (int s_dn = i; s_dn <= MIN(i + serc_rang, hei - 1); s_dn++)
						{
							if (comp[s_dn][j] == comp[i][j])
								edges_tmp.at<uchar>(s_dn, j) = 255;
							else
								break;
						}
					}
					else//up
					{
						for (int s_up = i; s_up >= MAX(i - serc_rang, 0); s_up--)
						{
							if (comp[s_up][j] == comp[i][j])
								edges_tmp.at<uchar>(s_up, j) = 255;
							else
								break;
						}
					}
				}//vertical
				if (image2.at<uchar>(i, MAX(j - 1, 0)) != image2.at<uchar>(i, MIN(j + 1, wid - 1)))//horizontal
				{
					if (image2.at<uchar>(i, MAX(j - 1, 0)) == ind_direct + 1)//right
					{
						for (int s_rt = j; s_rt <= MIN(j + serc_rang, wid - 1); s_rt++)
						{
							if (comp[i][s_rt] == comp[i][j])
								edges_tmp.at<uchar>(i, s_rt) = 255;
							else
								break;
						}
					}
					else//left
					{
						for (int s_lf = j; s_lf >= MAX(j - serc_rang, 0); s_lf--)
						{
							if (comp[i][s_lf] == comp[i][j])
								edges_tmp.at<uchar>(i, s_lf) = 255;
							else
								break;
						}
					}
				}//horizontal
			}//edges
		}
		//imshow("outliers", edges_tmp);


	for (int i = 0; i<hei; i++)
		delete[]comp[i];
	delete[]comp;
#pragma omp for
	for (int i = 0; i<hei; i++)
		for (int j = 0; j<wid; j++)
			if (edges_tmp.at<uchar>(i, j) == 255)
				occp.at<uchar>(i, j) = 1;
	return occp;
}

float colo(Mat &img, int x0, int y0, int x1, int y1)
{
	float coldf = abs(img.at<Vec3f>(y0, x0)[0] - img.at<Vec3f>(y1, x1)[0]);
	if (coldf<abs(img.at<Vec3f>(y0, x0)[1] - img.at<Vec3f>(y1, x1)[1]))
		coldf = abs(img.at<Vec3f>(y0, x0)[1] - img.at<Vec3f>(y1, x1)[1]);
	if (coldf<abs(img.at<Vec3f>(y0, x0)[2] - img.at<Vec3f>(y1, x1)[2]))
		coldf = abs(img.at<Vec3f>(y0, x0)[2] - img.at<Vec3f>(y1, x1)[2]);
	return coldf;
}
float spac(int x0, int y0, int x1, int y1)
{
	float dist = sqrt(float((x0 - x1)*(x0 - x1) + (y0 - y1)*(y0 - y1)));
	return dist;
}
bool isok(float Dc, float Dc1, float Ds)
{
	int to1 = 20, to2 = 6, L1 = 34, L2 = 17;
	if (Ds>L2 && Ds<L1)
	{
		if (!(Dc<to1 && Dc1<to1) || !(Ds<L1) || !(Dc<to2))
			return false;
	}
	else
	{
		if (!(Dc<to1 && Dc1<to1) || !(Ds<L1))
			return false;
	}
	return true;
}

void Fillhole(Mat image2)
{
	int height = image2.rows;
	int width = image2.cols;
	int thred = dRange / 7;
	uchar *pi;
	//for(int ci=0;ci<2;ci++)
	//{
#pragma omp for
		for (int y = 0; y<height; y++)
		{
			pi = image2.ptr<uchar>(y);
			for (int x = 0; x<width; x++)
			{
				int tem = pi[x];
				if (tem <= thred)
				{
					int lp, rp;
					lp = rp = 0;
					int lx, rx;
					lx = x;
					rx = x;
					if (lx - 1<0)
						lp = pi[lx];
					while ((lp <= thred) && (lx - 1 >= 0))
						lp = pi[--lx];
					if (rx + 1 >= width)
						rp = pi[rx];
					while ((rp <= thred) && (rx + 1<width))
						rp = pi[++rx];

					if (lp*rp <= thred*thred)
						pi[x] = lp>rp ? lp : rp;
					else
						pi[x] = lp<rp ? lp : rp;
				}
			}//x
		}//y
#pragma omp for
		for (int y = 0; y<height; y++)
		{
			pi = image2.ptr<uchar>(y);
			for (int x = 0; x<width; x++)
			{
				int tem = pi[x];
				if (tem <= thred)
				{
					int up, dp;
					up = dp = 0;
					int uy, dy;
					uy = y;
					dy = y;
					if (uy - 1<0)
						up = image2.at<uchar>(uy, x);
					while ((up <= thred) && (uy - 1 >= 0))
						up = image2.at<uchar>(--uy, x);
					if (dy + 1 >= height)
						dp = image2.at<uchar>(dy, x);
					while ((dp <= thred) && (dy + 1<height))
						dp = image2.at<uchar>(++dy, x);
					if (up*dp <= thred*thred)
						pi[x] = up>dp ? up : dp;
					else
						pi[x] = up<dp ? up : dp;
				}
			}//x
		}//y

	//}
}
float rm(int i, int j, int x, int y, Mat &color, Mat &disp, Mat &occp, int d, float gammai, float gammap)
{
	int m; float r;
	if (occp.at<uchar>(y, x) == 1)
		r = 0;
	else
	{
		float delt_c = 0, delt_g = 0;
		delt_c = sqrt(pow(float(color.at<Vec3b>(i, j)[0] - color.at<Vec3b>(y, x)[0]), 2) + pow(float(color.at<Vec3b>(i, j)[1] - color.at<Vec3b>(y, x)[1]), 2) + pow(float(color.at<Vec3b>(i, j)[2] - color.at<Vec3b>(y, x)[2]), 2));
		delt_g = sqrt(float((i - y)*(i - y) + (j - x)*(j - x)));
		r = exp(-(delt_c / gammai + delt_g / gammap));
	}

	if (disp.at<uchar>(y, x) == d)
		m = 1;
	else
		m = 0;

	return r*m;
}
void SGMF(Mat &disp, Mat &coloI_l, Mat &coloI_r, Mat &occp)
{
	int width = disp.cols;
	int height = disp.rows;
	vector<Mat> filtVM(dRange);
	for (int i = 0; i<dRange; i++)
		filtVM[i] = Mat::zeros(height, width, CV_32F);
	cout << "MWM.." << endl;

#pragma omp for
	for (int i = 0; i<height; i++)
	{
		uchar *pd = disp.ptr<uchar>(i);
		for (int j = 0; j<width; j++)
		{
			for (int d = 1; d <= dRange; d++)
			{
				if (pd[j] == d)
					filtVM[d - 1].at<float>(i, j) = (float)d;
			}
		}
	}
#pragma omp for
	for (int d = 0; d<dRange; d++)
	{
		float mmin = FLT_MAX, mmax = FLT_MIN;
		for (int i = 0; i<height; i++)
			for (int j = 0; j<width; j++)
			{
				float tmp = filtVM[d].at<float>(i, j);
				if (tmp<mmin)
					mmin = tmp;
				if (tmp>mmax)
					mmax = tmp;
			}
		filtVM[d] = (filtVM[d] - mmin) / (mmax - mmin);
	}

	//filtering
	int r = ceil(MAX(height, width) / 21.0);
	GuidedFilter2 gff(r, 0.0002);
	Mat dispOut = Mat::zeros(height, width, CV_8U);
	Mat imgAccum = Mat::zeros(height, width, CV_32F);
	Mat ImgL, ImgR;
	coloI_l.convertTo(ImgL, CV_32FC3, 1.0 / 255.0); coloI_r.convertTo(ImgR, CV_32FC3, 1.0 / 255.0);
#pragma omp for
	for (int d = 1; d <= dRange; d++)
	{
		Mat tmp = filtVM[d - 1];
		gff(tmp, filtVM[d - 1], ImgL, ImgR, d);
		add(imgAccum, filtVM[d - 1], imgAccum);
		for (int i = 0; i<height; i++)
			for (int j = 0; j<width; j++)
			{
				if ((imgAccum.at<float>(i, j)>0.5) && (dispOut.at<uchar>(i, j) == 0))
					dispOut.at<uchar>(i, j) = d;
			}
		//printf("%d of %d.\n", d, dRange);
	}

#pragma omp for
	for (int i = 0; i<height; i++)
	{
		for (int j = 0; j<width; j++)
		{
			if (occp.at<uchar>(i, j) = 1)
				disp.at<uchar>(i, j) = dispOut.at<uchar>(i, j);
		}
	}
	medianBlur(disp, disp, 3);
}

int _tmain(int argc, _TCHAR* argv[])
{
	Mat color = imread(leftPath);
	Mat color_r = imread(rightPath);
	int height = color.rows;
	int width = color.cols;
	Mat left_d(color.size(), CV_32FC3);
	color.convertTo(left_d, CV_32FC3, 1.0 / 255.0);
	Mat disparity_map = imread(dispPath,0);
	Mat disp = disparity_map / SCALE;

	Mat occPix = Findoutlier(disp);
	Fillhole(disp);
	SGMF(disp, color, color_r, occPix);

	disp *= SCALE;
	namedWindow("test");
	imshow("test", disp);
	waitKey();
	return 0;
}
