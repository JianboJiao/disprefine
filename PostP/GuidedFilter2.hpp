#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>

using namespace cv;
//just consider the condition with 3 channels
const int varIndexTable2[6][6]={0,		1,		2,		3,		4,		5,
													1,		6,		7,		8,		9,		10,
													2,		7,		11,	12,	13,	14,
													3,		8,		12,	15,	16,	17,
													4,		9,		13,	16,	18,	19,
													5,		10,	14,	17,	19,	20};
class GuidedFilter2{
private:
	Mat multiplyBuffer[21];
	Mat averageBuffer[21];

	Mat rgb[3];
	Mat rgb2[3];
	Mat mean[3];
	Mat mean2[3];
	Mat rgbtem[6],meantem[6];

	int r;
	float eps;

	void average(const Mat &src,Mat &dst){

		if (dst.size()!=src.size()||dst.type()!=CV_32F) {
			dst=Mat(src.size(),CV_32F);
		}
		int width = src.cols;
		int height = src.rows;
		Mat buffer;
		integral(src,buffer,CV_32F);
		for (int y=0;y<height;y++) {
			float *pd=dst.ptr<float>(y);
			for (int x=0;x<width;x++) {

				int xl=MAX(x-r,0);
				int xr=MIN(x+r,width-1);
				int yt=MAX(y-r,0);
				int yd=MIN(y+r,height-1);

				pd[x]=buffer.at<float>(yd+1,xr+1)
					-buffer.at<float>(yd+1,xl)
					-buffer.at<float>(yt,xr+1)
					+buffer.at<float>(yt,xl);
				pd[x]/=(xr-xl+1)*(yd-yt+1);
			}
		}
		buffer.release();
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
public:

	GuidedFilter2(int R,double Eps){
		r=R;
		eps=Eps;
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////
	void operator ()(const Mat &p,Mat &q,const Mat &image,const Mat &image2,int d){

		Mat mean_p;
		average(p,mean_p);

		Mat cov[6];
		Mat a[6];
		int width=image.cols;
		int height=image.rows;

		Mat image2_extend;
		Scalar valu=0.011;
		copyMakeBorder(image2,image2_extend,0,0,d,0,BORDER_CONSTANT,valu);
		Mat image2_buffer=image2_extend.colRange(0,width) ;

		split(image,rgb);
		split(image2_buffer,rgb2);
		for (int c=0;c<3;c++) {
			average(rgb[c],mean[c]);
			average(rgb2[c],mean2[c]);
		}

		//for(int c=0;c<6;c++)
		//{
		//	if(c<3)
		//	{
		//		rgbtem[c]=rgb[c];
		//		meantem[c]=mean[c];
		//	}
		//	else
		//	{
		//		rgbtem[c]=rgb2[c];
		//		meantem[c]=mean2[c];
		//	}
		//}
		rgbtem[0]=rgb[0];rgbtem[1]=rgb2[0];
		rgbtem[2]=rgb[1];rgbtem[3]=rgb2[1];
		rgbtem[4]=rgb[2];rgbtem[5]=rgb2[2];
		meantem[0]=mean[0];meantem[1]=mean2[0];
		meantem[2]=mean[1];meantem[3]=mean2[1];
		meantem[4]=mean[2];meantem[5]=mean2[2];

		Mat var[21];

#pragma omp parallel for 
		for (int c1=0;c1<6;c1++) {
			for (int c2=c1;c2<6;c2++) {
				int varIndex=varIndexTable2[c1][c2];

				multiply(rgbtem[c1],rgbtem[c2],multiplyBuffer[varIndex]);
				average(multiplyBuffer[varIndex],averageBuffer[varIndex]);

				multiply(meantem[c1],meantem[c2],multiplyBuffer[varIndex]);
				var[varIndex]= averageBuffer[varIndex]-multiplyBuffer[varIndex];
			}
		}
		//////////////////////////////////////////////////////////////////////////
#pragma omp parallel for
		for(int c=0;c<6;c++){

			multiply(rgbtem[c],p,multiplyBuffer[c]);

			average(multiplyBuffer[c],averageBuffer[c]);

			multiply(meantem[c],mean_p,multiplyBuffer[c]);
			cov[c]=averageBuffer[c]-multiplyBuffer[c];
			a[c]=Mat(p.size(),CV_32F);
		}

#pragma omp parallel for
		for(int y=0;y<height;y++){
			float cov_Ip[6];            
			float sigmaValue[6][6];
			Mat sigm;
			for(int x=0;x<width;x++){
				int sigmaIndex=y*width+x;
				for (int c=0;c<6;c++) {
					cov_Ip[c]=cov[c].at<float>(y,x);
				}
				for (int c1=0;c1<6;c1++) {
					for (int c2=0;c2<6;c2++) {
						sigmaValue[c1][c2]=var[varIndexTable2[c1][c2]].at<float>(y,x);
					}
				}
				Mat sigmaTemp=Mat(6,6,CV_32F,&sigmaValue[0][0])
					+eps*Mat::eye(6,6,CV_32F);
				invert(sigmaTemp,sigm);
				Mat buffer=Mat(1,6,CV_32F,&cov_Ip[0])*sigm;
				for (int c=0;c<6;c++) {
					a[c].at<float>(y,x)=buffer.at<float>(c);
				}
			}
		}

#pragma omp parallel for     
		for(int c=0;c<6;c++){
			multiply(a[c],meantem[c],multiplyBuffer[c]);
		}

		for(int c=0;c<6;c++){
			mean_p-=multiplyBuffer[c];
		}

		average(mean_p,q);

#pragma omp parallel for
		for(int c=0;c<6;c++){
			average(a[c],averageBuffer[c]);
			multiply(averageBuffer[c],rgbtem[c],multiplyBuffer[c]);
		}

		for(int c=0;c<6;c++){
			q+=multiplyBuffer[c];
		}

		mean_p.release();

#pragma omp parallel for
		for (int c=0;c<6;c++) {
			a[c].release();
			cov[c].release();
		}
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////
	void release(){
#pragma omp parallel for        
		for (int v=0;v<21;v++) {
			averageBuffer[v].release();
			multiplyBuffer[v].release();
		}
#pragma omp parallel for        
		for (int c=0;c<3;c++) {
			rgb[c].release();
			mean[c].release();
			rgb2[c].release();
			mean2[c].release();
		}
#pragma omp parallel for
		for(int c=0;c<6;c++)
		{
			rgbtem[c].release();
			meantem[c].release();
		}
	}
};
